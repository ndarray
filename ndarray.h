/*
 * N-dimensional strided array
 * Copyright 2018-2020 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NDARRAY_NDARRAY_H
#define NDARRAY_NDARRAY_H

#include <stddef.h>
#include <stdint.h>

/**
 * TODO:
 * custom allocators
 * destructors
 * other data types than double
 */

typedef struct NDASlice {
    ptrdiff_t start;
    ptrdiff_t end;
    ptrdiff_t step;
} NDASlice;

#define NDASLICE(start, end, step) ((NDASlice){ start, end, step })
#define NDASLICE_NULL ((NDASlice){ PTRDIFF_MAX })

typedef struct NDArrayInternal NDArrayInternal;

typedef struct NDArray {
    NDArrayInternal *priv;

    unsigned int     dims;

    double          *data;
    const size_t    *shape;
    const ptrdiff_t *stride;
} NDArray;

#define NDA_IDX1D(arr, x)       ((arr)->stride[0] * (x))
#define NDA_IDX2D(arr, x, y)    ((arr)->stride[0] * (y) + (arr)->stride[1] * (x))
#define NDA_IDX3D(arr, x, y, z) ((arr)->stride[0] * (z) + (arr)->stride[1] * (y) + (arr)->stride[0] * (x))

#define NDA_PTR1D(arr, x)       ((arr)->data + NDA_IDX1D(arr, x))
#define NDA_PTR2D(arr, x, y)    ((arr)->data + NDA_IDX2D(arr, x, y))
#define NDA_PTR3D(arr, x, y, z) ((arr)->data + NDA_IDX2D(arr, x, y, z))

#define NDARRAY_ALLOC_ZERO (1 << 0)

int ndarray_alloc(NDArray **arr, unsigned int dims,
                  const size_t * const size, unsigned int flags);
void ndarray_free(NDArray **arr);

int ndarray_wrap(NDArray **arr, unsigned int dims,
                 const size_t * const size, double *data,
                 const ptrdiff_t * const strides);

int ndarray_slice(NDArray **dst, NDArray *src,
                  const NDASlice * const slice);

int ndarray_copy(NDArray *dst, const NDArray *src);

#endif // NDARRAY_NDARRAY_H
