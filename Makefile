TARGET = libndarray.so

CFLAGS = -std=c99 -D_XOPEN_SOURCE=700 -fPIC -g -I.
TARGET_LDFLAGS = -Wl,--version-script=libndarray.v -shared -lm -pthread
TEST_LIBS = -lm -lpthread
CC = cc

OBJS =                       \
       ndarray.o             \

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) ${TARGET_LDFLAGS} -o $@ $(OBJS)

%.o: %.c
	$(CC) $(CFLAGS) -MMD -MF $(@:.o=.d) -MT $@ -c -o $@ $<

%.o: %.asm
	nasm -f elf -m amd64 -M $< > $(@:.o=.d)
	nasm -f elf -m amd64 -o $@ $<

clean:
	-rm -f *.o *.d *.pyc $(TARGET)

-include $(OBJS:.o=.d)

.PHONY: clean
